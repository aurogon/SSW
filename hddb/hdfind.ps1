param(
	$rootDir,
	$fileName,
	[switch]
	$objectOutput,
	[switch]
	$prettyObject
)
$exiting = $false;
$foundCount = 0;

$getItem = $false;

if($prettyObject) {
	$objectOutput = $true
}
if($objectOutput) {
	$getItem = $true
}
if(!$fileName) {
	if(!$rootDir) {
		echo 'Please specify file name to find'
		exit
	}
	$fileName = $rootDir
	$rootDir = ""#"$(get-location)"
}

##
function _GetItem($filePath) {
	#echo "@@@$filePath"
	$item = get-Item -force -literal $filePath
	if($prettyObject) {	
		$directoryName = $item.DirectoryName
		$name = $item.Name;
		$size= $item.length;
		if(!$directoryName) {
			$directoryName = $item.Parent.FullName
			$name = '[' + $name + ']'
			$size= $null;
		}
		new-object psobject -property ([ordered]@{
			Directory=$directoryName;
			Name=$name;
			Size=$size;
			Time=$item.lastwritetime
		})
	}
	else {
		$item
	}
}

##
$psi = New-Object System.Diagnostics.ProcessStartInfo
$psi.FileName = (Get-Command hddb.exe).Definition
$psi.WorkingDirectory = (Get-Location).Path
$psi.Arguments = "find `"$rootDir`" `"$fileName`""
#echo $psi.Arguments
#exit
$psi.UseShellExecute = $false

## Always redirect the input and output of the process.
## Sometimes we will capture it as binary, other times we will
## just treat it as strings.
$psi.RedirectStandardOutput = $true
$psi.RedirectStandardInput = $false

$startTick = [environment]::tickcount
$process = [System.Diagnostics.Process]::Start($psi)

$line = $process.StandardOutput.ReadLine()
$exiting = $false
try
{
	while(($line -ne $null) -and ($exiting -eq $false)) {

		if($getItem) {
			_GetItem $line
		}
		else {
			$line
		}
		++$foundCount;
		if($Host.UI.RawUI.KeyAvailable) {
			$c = [int]$Host.UI.RawUI.ReadKey("AllowCtrlC,IncludeKeyUp,NoEcho").Character;
			if(3 -eq $c) {
				$exiting = $true;
				break;
			}
		}
		$line = $process.StandardOutput.ReadLine()
	} 
}
finally {
	if($exiting) {
		write-host "`nCanceled.."
	}
	try {
		$process.Kill() 
	}
	catch {
		#let it be
	}
}
$ticks = [environment]::tickcount - $startTick;
write-host "`n($foundCount items found. $ticks ms elapsed)"
